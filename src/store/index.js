import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import repos from './modules/repos'
import reports from "./modules/reports";

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export const store = new Vuex.Store({
    state: {
        qtyHighValue: 500
    },
    mutations: {
        updateHighValue(state, value) {
            state.qtyHighValue = value;
        }
    },
    modules: {
        auth,
        repos,
        reports
    },
    strict: debug
})

export default store;